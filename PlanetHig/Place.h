//
//  Place.h
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Place : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;

-(id)initWithJSON; // not necessary?
-(id)initWithName:(NSString *)aName description:(NSString *)aDescription;

@end
