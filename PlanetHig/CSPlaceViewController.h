//
//  CSPlaceViewController.h
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "City.h"

@interface CSPlaceViewController : UIViewController

@property (strong, nonatomic) Place *place;
@property (strong, nonatomic) City *city;

@property (strong, nonatomic) UILabel *placeNameLabel;
@property (strong, nonatomic) UILabel *placeDescriptionLabel;

-(void)makeThisPlaceInteresting:(id)sender;

@end
