//
//  CSAppDelegate.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSAppDelegate.h"
#import "CSPlaceTableViewController.h"
#import "CSInterestingPlacesViewController.h"
#import "CSCityViewController.h"

@implementation CSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    CSPlaceTableViewController *placeTableViewController = [[CSPlaceTableViewController alloc]initWithStyle:UITableViewStylePlain];
    UINavigationController *placeNavController = [[UINavigationController alloc]
                                                 initWithRootViewController:placeTableViewController];
    
    CSInterestingPlacesViewController *interestingPlacesViewController = [[CSInterestingPlacesViewController alloc]initWithStyle:UITableViewStylePlain];
    UINavigationController *interestingPlacesNavController = [[UINavigationController alloc]
                                                  initWithRootViewController:interestingPlacesViewController];
    
    CSCityViewController *cityViewController = [[CSCityViewController alloc]init];
    UINavigationController *cityNavController = [[UINavigationController alloc]
                                                    initWithRootViewController:cityViewController];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UITabBarController *tabBarController = [[UITabBarController alloc]init];
    tabBarController.viewControllers = @[placeNavController, interestingPlacesNavController, cityNavController];
    self.window.rootViewController = tabBarController;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
