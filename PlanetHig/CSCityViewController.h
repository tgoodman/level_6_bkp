//
//  CSCityViewController.h
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@interface CSCityViewController : UIViewController

@property (strong, nonatomic) City *city;

@property (strong, nonatomic) UILabel *cityNameLabel;
@property (strong, nonatomic) UILabel *cityStateLabel;
@property (strong, nonatomic) UILabel *cityPopulationLabel;

@property (strong, nonatomic) UILabel *notesLabel;
@property (strong, nonatomic) UITextField *notesField;

-(void)dataRetrieved;
-(void)editPressed;

@end
