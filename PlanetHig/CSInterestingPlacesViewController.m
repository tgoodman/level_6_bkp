//
//  CSInterestingPlacesViewController.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSInterestingPlacesViewController.h"
#import "CSPlaceViewController.h"

@interface CSInterestingPlacesViewController ()

@end

@implementation CSInterestingPlacesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Favorites";
        self.tabBarItem.image = [UIImage imageNamed:@"favorites2"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.city = [City getCity];
    //NSLog(@"self.city.interestingPlaces.count = %i", self.city.interestingPlaces.count);
}
-(void)viewWillAppear:(BOOL)animated
{
    self.city = [City getCity];
    [self.tableView reloadData];
    NSLog(@"self.city.interestingPlaces.count = %i", self.city.interestingPlaces.count);
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.city.interestingPlaces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //NSLog(@"Cell created!");
    
    cell.textLabel.text = [self.city.interestingPlaces[indexPath.row] name];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CSPlaceViewController *placeVC = [[CSPlaceViewController alloc]init];
    placeVC.place = self.city.interestingPlaces[indexPath.row];
    [self.navigationController pushViewController:placeVC animated:YES];
    
}

@end
