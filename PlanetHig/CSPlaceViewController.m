//
//  CSPlaceViewController.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSPlaceViewController.h"

@interface CSPlaceViewController ()

@end

@implementation CSPlaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
	// Create data containers
    self.placeNameLabel = [[UILabel alloc]init];
    self.placeNameLabel.frame = CGRectMake(20,100,280,40);
    [self.view addSubview:self.placeNameLabel];
    
    self.placeDescriptionLabel = [[UILabel alloc]init];
    self.placeDescriptionLabel.frame = CGRectMake(20,200,280,20);
    [self.view addSubview:self.placeDescriptionLabel];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // Set data in containers
    self.placeNameLabel.text = self.place.name;
    self.placeDescriptionLabel.text = self.place.description;
    
    UIButton *interestingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    interestingButton.frame = CGRectMake(60,300,200,44);
    [interestingButton setTitle:@"Mark as Interesting" forState:UIControlStateNormal];
    [interestingButton addTarget:self action:@selector(makeThisPlaceInteresting:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:interestingButton];
}

-(void)makeThisPlaceInteresting:(id)sender
{
    self.city = [City getCity];
    NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:self.city.interestingPlaces];
    [tempArray addObject:self.place];
    self.city.interestingPlaces = [[NSArray alloc]initWithArray:tempArray];
    
    // Everytime you modify the state of the model, archive it.
    [City saveCity:self.city];
}


@end
