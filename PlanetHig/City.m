//
//  City.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "City.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworking.h"

@implementation City

-(id)init
{
    self = [self initWithJSON];
    return self;
}

-(id)initWithJSON
{
    self = [super init];
    if(self) {
        // Create a NSURL set to our endpoint
        NSURL *url = [[NSURL alloc]initWithString:@"https://s3.amazonaws.com/TMG_JSON/city.json"];
        
        // Create a NSURLRequest with our NSURL
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
        
        // Create a AFJSONRequestOperation which will do the call to the internet
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                             JSONRequestOperationWithRequest:request
                                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
                                                 self.name = JSON[@"name"];
                                                 self.state = JSON[@"state"];
                                                 self.population = JSON[@"population"];
                                                 self.notes = @"no notes yet";
                                                 self.interestingPlaces = nil;
                                                 [City saveCity:self];
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"initWithJSONFinishedLoading"
                                                                                                     object:nil];
                                             }
                                             failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                 NSLog(@"NSError: %@", error.localizedDescription);
                                             }];
        [operation start];
        self.notes = @"no notes yet"; // JSON won't have this
    }
    return self;
}

// Unbox data
-(City *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.state = [aDecoder decodeObjectForKey:@"state"];
        self.population = [aDecoder decodeObjectForKey:@"population"];
        self.notes = [aDecoder decodeObjectForKey:@"notes"];
        self.interestingPlaces = [aDecoder decodeObjectForKey:@"interestingPlaces"];
    }
    return self;
}

// Box data
-(void)encodeWithCoder:(NSCoder *)anEncoder
{
    [anEncoder encodeObject:self.name forKey:@"name"];
    [anEncoder encodeObject:self.state forKey:@"state"];
    [anEncoder encodeObject:self.population forKey:@"population"];
    [anEncoder encodeObject:self.notes forKey:@"notes"];
    [anEncoder encodeObject:self.interestingPlaces forKey:@"interestingPlaces"];
}


+(NSString *)getPathToArchive
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [paths objectAtIndex:0];
    return [docsDir stringByAppendingPathComponent:@"city.model"];
}

+(void)saveCity:(City *)aCity
{
    [NSKeyedArchiver archiveRootObject:aCity toFile:[City getPathToArchive]];
}

+(City *)getCity
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[City getPathToArchive]];
}

@end
