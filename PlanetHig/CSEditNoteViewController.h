//
//  CSEditNoteViewController.h
//  PlanetHig
//
//  Created by TMG on 10/19/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@interface CSEditNoteViewController : UIViewController <UITextFieldDelegate>


@property (strong, nonatomic) City *city;

@property (strong, nonatomic) UILabel *cityNameLabel;
@property (strong, nonatomic) UILabel *cityStateLabel;
@property (strong, nonatomic) UITextField *notesField;

-(void)savePressed;
-(void)dismissPressed;


@end
