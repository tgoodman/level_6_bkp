//
//  CSPlaceTableViewController.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSPlaceTableViewController.h"
#import "Place.h"
#import "CSPlaceViewController.h"
#import "AFNetworking.h"
#import "AFJSONRequestOperation.h"


@interface CSPlaceTableViewController ()

@end

@implementation CSPlaceTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.tabBarItem.image = [UIImage imageNamed:@"feed"];
        self.title = @"Place";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [[NSURL alloc]initWithString:@"https://s3.amazonaws.com/TMG_JSON/places.json"];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
                                             NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                                             for (NSDictionary *oneDictionary in JSON) {
                                                 Place *newPlace = [[Place alloc]initWithName:oneDictionary[@"name"]
                                                                                  description:oneDictionary[@"description"]];
                                                 [tempArray addObject:newPlace];
                                             }
                                             self.places  = [[NSArray alloc] initWithArray:tempArray];
                                             NSLog(@"self.places.count = %d", self.places.count);
                                             [self.tableView reloadData];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             NSLog(@"NSError: %@", error.localizedDescription);
                                         }];
    [operation start];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.city = [City getCity];
    NSLog(@"self.city.interestingPlaces.count = %i", self.city.interestingPlaces.count);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.places.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.places[indexPath.row] name];
    cell.detailTextLabel.text = [self.places[indexPath.row] description];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Pass in a Place model object to a new PlaceViewController
    CSPlaceViewController *placeVC = [[CSPlaceViewController alloc]init];
    placeVC.place = self.places[indexPath.row];
    [self.navigationController pushViewController:placeVC animated:YES];

}

@end
