//
//  Place.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "Place.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworking.h"

@implementation Place

-(id)init
{
    self = [self initWithJSON];
    return self;
}

-(id)initWithJSON
{
    self = [super init];
    if(self) {
        NSURL *url = [[NSURL alloc]initWithString:@"https://s3.amazonaws.com/TMG_JSON/places.json"];
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
        
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                             JSONRequestOperationWithRequest:request
                                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
                                                 self.name = JSON[@"name"];
                                                 self.description = JSON[@"description"];
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"initWithJSONFinishedLoading"
                                                                                                     object:nil];
                                             }
                                             failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                 NSLog(@"NSError: %@", error.localizedDescription);
                                             }];
        
        [operation start];
    }
    //NSLog(@"CREATED PLACE");
    return self;
}

-(id)initWithName:(NSString *)aName description:(NSString *)aDescription
{
    self = [super init];
    if(self) {
        self.name = aName;
        self.description = aDescription;
    }
    //NSLog(@"CREATED PLACE[%@]", self.name);
    return self;
}

// Unbox data
-(Place *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.description = [aDecoder decodeObjectForKey:@"description"];
    }
    return self;
}

// Box data
-(void)encodeWithCoder:(NSCoder *)anEncoder
{
    [anEncoder encodeObject:self.name forKey:@"name"];
    [anEncoder encodeObject:self.description forKey:@"description"];
}

@end
