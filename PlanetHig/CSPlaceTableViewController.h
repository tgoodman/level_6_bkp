//
//  CSPlaceTableViewController.h
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "City.h"

@interface CSPlaceTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *places;
@property (strong, nonatomic) City *city;

@end
