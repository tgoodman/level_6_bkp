//
//  CSCityViewController.m
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSCityViewController.h"
#import "CSEditNoteViewController.h"

@interface CSCityViewController ()

@end

@implementation CSCityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{ 
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"City";
        self.tabBarItem.image = [UIImage imageNamed:@"city"];
        
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                   target:self
                                                                                   action:@selector(editPressed)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.cityNameLabel = [[UILabel alloc]init];
    self.cityNameLabel.frame = CGRectMake(20,75,280,40);
    [self.view addSubview:self.cityNameLabel];
    
    self.cityStateLabel = [[UILabel alloc]init];
    self.cityStateLabel.frame = CGRectMake(20,100,280,40);
    [self.view addSubview:self.cityStateLabel];
    
    self.notesLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,200,280,40)];
    [self.view addSubview:self.notesLabel];
    
    self.city = [[City alloc]init];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataRetrieved)
                                                 name:@"initWithJSONFinishedLoading"
                                               object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.city = [City getCity];
    self.notesLabel.text = self.city.notes;
}

-(void)editPressed
{
    CSEditNoteViewController *editNoteVC = [[CSEditNoteViewController alloc]init];
    editNoteVC.city = self.city;
    [self presentViewController:editNoteVC animated:YES completion:nil];
}

-(void)dataRetrieved
{
    self.cityNameLabel.text = self.city.name;
    self.cityStateLabel.text = self.city.state;
    self.cityPopulationLabel.text = self.city.population;
}

@end
