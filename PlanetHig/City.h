//
//  City.h
//  PlanetHig
//
//  Created by TMG on 10/18/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *population;
@property (strong, nonatomic) NSString *notes;
@property (strong, nonatomic) NSArray *interestingPlaces;

-(id)initWithJSON;
+(City *)getCity;
+(void)saveCity:(City *)aCity;

@end
