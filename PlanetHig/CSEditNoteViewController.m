//
//  CSEditNoteViewController.m
//  PlanetHig
//
//  Created by TMG on 10/19/13.
//  Copyright (c) 2013 Personal. All rights reserved.
//

#import "CSEditNoteViewController.h"

@interface CSEditNoteViewController ()

@end

@implementation CSEditNoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
	
    // !!! YOU ARE PASSING IN THE MODEL - NO NEED TO RE-ALLOCATE/INIT !!!
    // self.city = [[City alloc]init];
    
    self.cityNameLabel = [[UILabel alloc]init];
    self.cityNameLabel.frame = CGRectMake(20,75,280,40);
    self.cityNameLabel.text = self.city.name;
    [self.view addSubview:self.cityNameLabel];
    
    self.cityStateLabel = [[UILabel alloc]init];
    self.cityStateLabel.frame = CGRectMake(20,100,280,40);
    self.cityStateLabel.text = self.city.state;
    [self.view addSubview:self.cityStateLabel];
    
    self.notesField = [[UITextField alloc]initWithFrame:CGRectMake(15,200,290,30)];
    self.notesField.delegate = self;
    self.notesField.keyboardType = UIKeyboardTypeDefault;
    self.notesField.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:self.notesField];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    saveButton.frame = CGRectMake(15,250,290,50);
    [saveButton setTitle:@"Save Note" forState:UIControlStateNormal];
    [self.view addSubview:saveButton];
    [saveButton addTarget:self action:@selector(savePressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    dismissButton.frame = CGRectMake(15,300,290,50);
    [dismissButton setTitle:@"Don't Save" forState:UIControlStateNormal];
    [self.view addSubview:dismissButton];
    [dismissButton addTarget:self action:@selector(dismissPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                  initWithTarget:self
                                  action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteringBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteringForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.city = [City getCity];
    self.notesField.text = self.city.notes;  // This is so we have something to edit.
}

-(void)savePressed
{
    self.city.notes = self.notesField.text;
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard
{
    [self.notesField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Update the City model with the text in notesField.
    self.city.notes = textField.text;
    [textField resignFirstResponder];
    [City saveCity:self.city];
    
    return YES;
}


-(void)enteringBackground
{
    [City saveCity:self.city];
}

-(void)enteringForeground
{
    [City getCity];
}

@end
